const createApp = require('../app')

const defaultImplementations = {
  business: '../components/business',
  web: '../components/web',
  config: '../components/config',
  data: '../components/postgres-data'
}

const buildApp = (implementationMap) => {
  const implementations = Object.assign(defaultImplementations, implementationMap);

  const createBusinessComponent = require(implementations.business)
  const createWebComponent = require(implementations.web)
  const createConfiguration = require(implementations.config)
  const createPostgresDataComponent = require(implementations.data)

  const configurationComponent = createConfiguration()
  const businessComponent = createBusinessComponent(configurationComponent)

  const dataComponent = createPostgresDataComponent(configurationComponent)

  return createApp(configurationComponent, businessComponent, createWebComponent(configurationComponent, businessComponent), dataComponent)
}

module.exports = buildApp