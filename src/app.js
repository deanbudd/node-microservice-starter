let createApplication = (configComponent, busComponent, web, data) => {
  let businessComponent = busComponent
  let configurationComponent = configComponent
  let webComponent = web
  let dataComponent = data

  const start = () => {
    console.log('starting components')
    configurationComponent.start()
    dataComponent.start()
    businessComponent.start()
    webComponent.start()
  }

  const stop = () => {
    console.log('stopping components')
    configurationComponent.stop()
    dataComponent.stop()
    businessComponent.stop()
    webComponent.stop()
  }

  return {
    start, stop, configurationComponent, businessComponent, webComponent, dataComponent
  }
}

module.exports = createApplication
