const buildApp = require('./factory/app-factory')

const application = buildApp({})
application.start()
