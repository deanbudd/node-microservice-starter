const {Client} = require('pg')

let createPostgresDataComponent = (config) => {
  let client

  const doBusinessStuff = (message) => {
    console.log('postgres data component received : %s', message)
  }

  const start = () => {
    console.log('Starting postgres data component with config : ' + JSON.stringify(config.configuration().database))

    client = new Client(config.configuration().database)

    client.connect((err) => {
      if (err) {
        console.error('connection error', err.stack)
      } else {
        console.log('connected')
      }
    })
  }

  const stop = () => {
    console.log('Stopping postgres data component')
    client.end()
  }

  return {
    doBusinessStuff, start, stop
  }
}

module.exports = createPostgresDataComponent
