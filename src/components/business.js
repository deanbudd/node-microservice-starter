let createBusinessComponent = () => {

  const doBusinessStuff = (message) => {
    console.log('business component received : %s', message)
  }

  const start = () => {
    console.log('Starting business component')
  }

  const stop = () => {
    console.log('Stopping business component')
  }

  return {
    doBusinessStuff, start, stop
  }
}

module.exports = createBusinessComponent
