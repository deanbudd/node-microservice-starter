let createConfigurationComponent = () => {
  const env = process.env.NODE_ENV || 'development';

  console.log('configuration component environment : ' + env)

  const file = '../config/' + env + '.config.json'
  let conf

  const configuration = () => {
    console.log('configuration retreived...')
    return conf
  }

  const start = () => {
    console.log('Starting configuration component')
    conf = require(file)
  }

  const stop = () => {
    console.log('Stopping configuration component')
  }

  return {
    configuration, start, stop
  }
}

module.exports = createConfigurationComponent
