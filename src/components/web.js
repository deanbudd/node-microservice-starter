let createWebComponent = (configurationComponent, businessComponent) => {

  const Koa = require('koa')
  const Router = require('koa-router')
  const body = require('koa-json-body')
  const router = new Router()

  const app = new Koa()
  let server

  console.log('Start of createWebComponent')

  router.get('/health', function (ctx, next) {
    ctx.status = 200
    ctx.body = 'Hello World!'
  })

  router.post('/receive', function (ctx, next) {
    ctx.status = 201
    businessComponent.doBusinessStuff(ctx.request.body.someparameter)
  })

  app.use(body({limit: '10kb', fallback: true}))
  app.use(router.routes()).use(router.allowedMethods())

  const start = () => {
    console.log('Starting web server with ' + configurationComponent.configuration().web.port)
    server = app.listen(configurationComponent.configuration().web.port)
  }

  const stop = () => {
    console.log('Stopping web server')
    server.close()
  }

  console.log('End of createWebComponent.')

  return {
    start, stop
  }
}

module.exports = createWebComponent
