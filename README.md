# Node µService Starter

## Features ##

Basic Component Level Dependency Injection

Web Server is a component of the application rather than the application being a component of the web server. This is the "app is the container" philosophy

`main` is responsible for wiring the app together and injecting all relevant dependencies and then starting it. This means most of the substantial anti-pattern `require` stuff is isolated to where the application is built.

`app` is the application which is effectively a component container. It's only responsibilities are to start and stop its components. This gives us our polymorphism where we can wire in different implementations of our components, especially for testing. This allows to manage dependencies cleanly instead of doing hack jobs on the `require` mechanism using hacks like `proxyquire`

## Testing ##

Included is a basic example of API and Mock Testing.

## Database ##

This example app requires a connection to a postgres database

`docker run --name node-microservice-postgres -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres`

## Docker Compose ##

To fire up both containers in one go..

`docker-compose up`
