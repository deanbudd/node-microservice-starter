process.env.NODE_ENV = 'development'

const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = chai.expect

const buildApp = require('../src/factory/app-factory')
const application = buildApp({business: '../../test/test-doubles/mock-business'})

chai.use(chaiHttp)

describe('suite', () => {
  before('start application', () => {
    application.start()
  })

  describe('business component', () => {
    it('should receive a message from the web layer', (done) => {
      const expectedMessage = 'dean'

      chai.request('http://localhost:' + application.configurationComponent.configuration().web.port)
          .post('/receive')
          .send({someparameter: expectedMessage})
          .end(function (error, response) {
            expect(response.status).to.equal(201)
            expect(application.businessComponent.recordedMessage()).to.equal(expectedMessage)
            done()
          })
    })
  })

  after('stop application', () => {
    application.stop()
  })
})