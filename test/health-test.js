process.env.NODE_ENV = 'development'

const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = chai.expect

const buildApp = require('../src/factory/app-factory')
const application = buildApp({})

chai.use(chaiHttp)

describe('suite', () => {
  before('start application', () => {
    application.start()
  })

  describe('health', () => {
    it('endpoint returns http 200', (done) => {
      chai.request('http://localhost:' + application.configurationComponent.configuration().web.port)
          .get('/health')
          .end(function (error, response) {
            expect(response.status).to.equal(200)
            done()
          })
    })
  })

  after('stop application', () => {
    application.stop()
  })
})
