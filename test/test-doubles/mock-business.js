let businessComponent = () => {
  let rm = null

  const doBusinessStuff = (message) => {
    console.log('MOCK BUSINESS COMPONENT RECORDING MESSAGE : %s', message)
    rm = message
  }

  const recordedMessage = () => {
    return rm
  }

  const start = () => {
    console.log('Starting mock business component')
  }

  const stop = () => {
    console.log('Stopping mock business component')
  }

  return {
    doBusinessStuff, recordedMessage, start, stop
  }
}

module.exports = businessComponent